# Projet Arbre Binaire & Compression de Huffman

# Compiler le code
On va d'abord nettoyer le dossier du projet afin d'effectuer une compilation propre du logiciel avec la commande:
    
    make clean
Ensuite nous allons compiler le code du projet avec la commande:

    make all

# Lancer le code

## Compression de fichier
Pour effectuer la compression d'un fichier nous allons taper la commande suivante dans le terminal en enlevant les chevrons:

    ./bin/huffman compress <nom du fichier>
Cela va donc créer 2 fichier; un premier "nom du fichier".huff ainsi qu'un second "nom du fichier".huff.tree 
## Décompression de fichier
Pour effectuer la décompression d'un fichier nous allons taper la commande suivante dans le terminal en enlevant les chevrons:

    ./bin/huffman decompress <nom du fichier>.huff
Cela va donc printf a la fin de la décompression le contenu décompressé du fichier
# Structure du projet
## Dossiers
### Include
Le dossier include comprend tout les fichiers .h du projet
##### Fichiers
 1. binary_tree.h
 2. huffman_tree.h
 3. weight.h

Le premier fichier contient tout les prototypes nécessaires a la création ainsi que l'affichage d'un arbre binaire standard.

Le deuxième fichier contient tout les prototypes nécessaires a la création ainsi que l'affichage d'un arbre de compression d'Huffman.

Le troisième fichier contient tout les prototypes pour créer une fonction de poids générique pour la création de l'arbre de Huffman.
### src
Le dossier src comprend tout les fichiers .c du projet
##### Fichiers
 1. main.c
 2. binary_tree.c
 3. huffman_tree.c
 4. weight.c

Le premier fichier est le fichier principal du logiciel, permettant ainsi son démarrage, ainsi que d'effectuer les actions de compression décompression.

Le deuxième  fichier contient toutes les fonctions nécessaires a la création ainsi que l'affichage d'un arbre binaire standard.

Le troisième fichier contient toutes les fonctions nécessaires a la création ainsi que l'affichage d'un arbre de compression d'Huffman.

Le quatrième fichier contient toutes les fonctions de poids générique pour la création de l'arbre de Huffman.
