#include "huffman_tree.h"
#define BIT(x,n) (((x) >> (n)) & 1)
tpl create_tuple(char letter, int occ)
{
		tpl tuple = (tpl)malloc(sizeof(struct tuple));
		tuple->letter = letter;
		tuple->occurence = occ;
		return tuple;
}

nd_h create_node_huffman(tpl values)
{
		nd_h node_h = (nd_h)malloc(sizeof(struct node_huffman));
		node_h->left_node = NULL;
		node_h->right_node = NULL;
		node_h->values = values;
		return node_h;
}

int length_string(char* string)
{
		int length = 0;
		while ( string[length] != '\0')
				length++;
		return length;
}

int is_in_string(char* string, char letter, int size)
{
		for (int i = 0; i < size; i++)
		{
				if (string[i] == letter)
						return 1;
		}
		return 0;
}

int different_letters(char* string)
{
		int res = 0;
		int size = length_string(string);
		char* dif = (char*)calloc(size, sizeof(char));
		for (int i = 0; i < size; i++)
		{
				if (is_in_string(dif, string[i], size) == 0) {
						dif[res] = string[i];
						res++;
				}
		}
		free(dif);
		dif = NULL;
		return res;
}

int not_in_tuples(tpl* tuples, char letter, int size)
{
		for (int i = 0; i < size; i++)
		{
				if (tuples[i] != NULL && tuples[i]->letter == letter)
						return 0;
		}
		return 1;
}

void add_one(tpl* tuples, char letter, int size)
{
		for (int i = 0; i < size; i++) {
				if (tuples[i] != NULL && tuples[i]->letter == letter)
				{
						tuples[i]->occurence++;
						return;
				}
		}
}

tpl* create_tuples(char* string, int size)
{
		int size_string = length_string(string);
		tpl* tuples = (tpl*)malloc(size*sizeof(struct tuple));
		int index_of_tuples = 0;
		for (int i = 0; i < size_string; i++)
		{
				if ( not_in_tuples(tuples, string[i], index_of_tuples) == 1 )
				{
						tuples[index_of_tuples] = create_tuple(string[i], 1);
						index_of_tuples++;
				}
				else
						add_one(tuples, string[i], size);
		}
		return tuples;
}

int minimal_occurence(nd_h* nodes, int* size)
{
		int minimal = nodes[0]->values->occurence;
		for(int i = 1; i < *size; i++)
		{
				if(nodes[i]->values->occurence < minimal)
				{
						minimal = nodes[i]->values->occurence;
				}
		}
		return minimal;
}


void merge_smallest(nd_h* nodes, int* size, int(*weight_ptr)(nd_h))
{
		nd_h smallest1 = nodes[0];
		int index1 = 0;
		int weight_small1 = (*weight_ptr)(smallest1);
		nd_h smallest2 = nodes[1];
		int index2 = 1;
		int weight_small2 = (*weight_ptr)(smallest2);

		int minimal_nodes_occ = minimal_occurence(nodes, size); 

		printf("Occurence: %d\n", nodes[0]->values->occurence);

		for(int i = 0; i < *size; i++)
		{
				if((minimal_nodes_occ == nodes[i]->values->occurence && (*weight_ptr)(nodes[i]) < weight_small1)
								|| (minimal_nodes_occ == nodes[i]->values->occurence && (*weight_ptr)(nodes[i]) < weight_small2))
				{
						if(minimal_nodes_occ == nodes[i]->values->occurence
										&& (*weight_ptr)(nodes[i]) < weight_small1)
						{
								smallest2 = smallest1;
								index2 = index1;
								weight_small2 = weight_small1;
								smallest1 = nodes[i];
								index1 = i;
								weight_small1 = (*weight_ptr)(nodes[i]);
						}

						if(minimal_nodes_occ == nodes[i]->values->occurence 
										&& (*weight_ptr)(nodes[i]) < weight_small2 
										&& nodes[i] != smallest1)
						{
								smallest2 = nodes[i];
								index2 = i;
								weight_small2 = (*weight_ptr)(nodes[i]);
						}    
				}


		}

		tpl tempo = create_tuple('\0', smallest1->values->occurence + smallest2->values->occurence);
		nd_h node_tempo = create_node_huffman(tempo);
		node_tempo->left_node = smallest1;
		node_tempo->right_node = smallest2;

		nodes[index1] = node_tempo;
		nodes[index2] = NULL;

		for (int j = index2+1; j < *size; j++)
		{
				nodes[j-1] = nodes[j];
				nodes[j] = NULL;
		}
		*size = *size - 1;
}

nd_h* tuples_to_nodes(tpl* tuples, int size){
		nd_h* nodes = (nd_h*)malloc(size*sizeof(struct node_huffman));
		for (int i = 0; i < size; i++) {
				nodes[i] = create_node_huffman(tuples[i]);
		}
		free(tuples);
		tuples = NULL;
		return nodes;
}

nd_h *create_tree_huffman(char* string, int(*weight_ptr)(nd_h))
{
		int size_list = different_letters(string);
		tpl* tuples = create_tuples(string, size_list);
		nd_h* nodes = tuples_to_nodes(tuples, size_list);
		while (size_list > 1)
		{
				merge_smallest(nodes, &size_list, weight_ptr);
		}
		return nodes;
}


void print_tree_huffman(nd_h tree, int space)
{
		if(tree == NULL)
				return;

		space += 10;

		print_tree_huffman(tree->right_node, space);

		printf("\n");
		for(int i = 10; i < space; i++)
		{
				printf(" ");
		}
		if(' ' == tree->values->letter)
		{
				printf("(Space, %d)", tree->values->occurence);
		}
		else if(tree->values->letter == '\0')
		{
				printf("(%d)", tree->values->occurence);
		}
		else if(tree->values->letter == '\a')
		{
				printf("(\\a, %d)", tree->values->occurence);
		}
		else if(tree->values->letter == '\b')
		{
				printf("(\\b, %d)", tree->values->occurence);
		}
		else if(tree->values->letter == '\f')
		{
				printf("(\\f, %d)", tree->values->occurence);
		}
		else if(tree->values->letter == '\n')
		{
				printf("(\\n, %d)", tree->values->occurence);
		}
		else if(tree->values->letter == '\r')
		{
				printf("(\\r, %d)", tree->values->occurence);
		}
		else if(tree->values->letter == '\t')
		{
				printf("(\\t, %d)", tree->values->occurence);
		}
		else if(tree->values->letter == '\v')
		{
				printf("(\\v, %d)", tree->values->occurence);
		}
		else
		{
				printf("(%c, %d)", tree->values->letter, tree->values->occurence);
		}
		print_tree_huffman(tree->left_node, space);
}

void free_tree_huffman(nd_h *tree)
{
		if(tree != NULL && *tree != NULL)
		{
				free_tree_huffman(&((*tree)->left_node));
				(*tree)->left_node = NULL;
				free_tree_huffman(&((*tree)->right_node));
				(*tree)->right_node = NULL;
				free((*tree)->values);
				(*tree)->values = NULL;
				free(*tree);
				*tree = NULL;
		}
}

char* readfile(char* filepath, ssize_t *contentlength)
{
		char *text = NULL;
		FILE *file = fopen(filepath, "r");
		if(file == NULL)
		{
				return NULL;
		}

		char buffer[BUFSIZE];
		ssize_t len = 0;
		ssize_t text_length = 0;
		char* tmp = NULL;
		while((len = (ssize_t)fread(buffer, 1, BUFSIZE, file)) > 0)
		{
				tmp = (char *)malloc(text_length + len + 1);
				memset(tmp, 0, text_length + len + 1);
				if(text != NULL)
						memcpy(tmp, text, text_length);
				memcpy(tmp + text_length, buffer, len);
				text_length += len;		
				if(text != NULL)
						free(text);
				text = tmp;
				tmp = NULL;
		}
		if(len == 0 && ferror(file) && text != NULL)
				free(text);
		fclose(file);
		*contentlength = text_length;
		return (len < 0 ? NULL : text);
}

int is_leaf(nd_h node)
{
		if(node->left_node != NULL || node-> right_node != NULL)
				return 0;
		return 1;
}

int depth(nd_h tree)
{
		if(tree == NULL)
				return 0;
		else
		{
				int lDepth = depth(tree->left_node);
				int rDepth = depth(tree->right_node);

				if(lDepth > rDepth)
						return(lDepth+1);
				else
						return(rDepth+1);
		}	
}

lcomp *list_compression(nd_h tree, char direction, int *len, int depth)
{
		//lcomp *res = (lcomp *)malloc(different_letters(text) * sizeof(struct letter_compression));
		if(is_leaf(tree) == 1)
		{
				lcomp *res = malloc(sizeof(lcomp));
				res->compression= malloc(depth+1 * sizeof(char));
				memset(res->compression, '\0', depth+1);
				res->letter = tree->values->letter;
				res->compression[0] = direction;
				*len = 1;
				return res;
		}
		int lenright = 0;
		int lenleft = 0;
		lcomp *right = list_compression(tree->right_node, '1', &lenright, depth);
		lcomp *left = list_compression(tree->left_node, '0', &lenleft , depth);
		*len = lenright + lenleft;
		lcomp *concat = (lcomp *)malloc(*len * sizeof(lcomp));	

		for(int i = 0; i < *len; i++)
		{
				if(i < lenright)
				{
						int strlength = strlen(right[i].compression);
						right[i].compression[strlength] = direction;
						concat[i] = right[i];
				}
				else
				{
						int j = i - lenright;
						int strlength = strlen(left[j].compression);
						left[j].compression[strlength] = direction;
						concat[i] = left[j];
				}
		}

		free(right);
		free(left);
		return concat;
}


void print_list_compression(lcomp *res, int len)
{
		for(int i = 0; i < len; i++)
		{
				printf("\n%c : %s", res[i].letter, res[i].compression);
		}
}

void reverse(char *str)
{
		int length = strlen(str);
		for(int i = 0; i < length/2; i++)
		{
				char tmp = str[length - i - 1];
				str[length - i - 1] = str[i];
				str[i] = tmp;  
		}
}

void reverse_compressions(lcomp *compressions, int len)
{
		for(int i = 0; i < len; i++)
		{
				reverse(compressions[i].compression);
		}
}


void free_list_compression(lcomp *compressions, int len)
{
		for(int i = 0; i < len; i++)
		{
				free(compressions[i].compression);
				compressions[i].compression = NULL;
		}
		free(compressions);
		compressions = NULL;
}

unsigned long getCompSize(lcomp *compressions, int len, char *text)
{
		unsigned long size = 0;
		int textlength = strlen(text);

		for(int i = 0; i < textlength; i++)
		{	
				for (int j = 0; j < len; j++)
				{
						if(text[i] == compressions[j].letter)
						{	
								size += strlen(compressions[j].compression);
								break;
						}
				}	
		}
		printf("\nTaille: %ld\n", size);
		return size;
}

char *prepareCompressed(lcomp *compressions, int len, unsigned long *compressionSize, int *arraySize, char *text)
{
		*compressionSize = getCompSize(compressions, len, text);
		int offset = 0;
		*arraySize = (*compressionSize) / 8 + ((*compressionSize) % 8 > 0 ? 1 : 0);
		char *compressed = (char *)malloc(*arraySize);
		memset(compressed, 0, *arraySize);
		int textlength = strlen(text);    

		for(int k = 0; k < textlength; k++)
		{
				for (int i = 0; i < len; i++)
				{
						if(text[k] == compressions[i].letter)
						{
								int compressedCharSize = strlen(compressions[i].compression);
								for (int j = 0; j < compressedCharSize; j++)
								{
										int offsetAndBit = j + offset;
										int currentByte = offsetAndBit / 8;
										int byteOffset = offsetAndBit % 8;
										if (compressions[i].compression[j] == '1')
										{
												compressed[currentByte] |= (1 << byteOffset);
										}
								}
								offset += compressedCharSize;
						}
				}
		}
		return compressed;
}

void write_in_file(char *path, char *buffer, unsigned long compressionSize, int arraySize)
{
		FILE *compressed = fopen(path, "w");

		fwrite(&compressionSize, 1, sizeof(unsigned long), compressed);
		fwrite(buffer, 1, arraySize, compressed);

		fclose(compressed);
}

int nb_nodes(nd_h tree)
{
		if(tree == NULL)
		{
				return 0;
		}
		return 1 + nb_nodes(tree->left_node) + nb_nodes(tree->right_node);
}

void ft_fill(nd_h tree, ft *buffer, int index)
{
		if (tree != NULL) {
				buffer[index].occurence = tree->values->occurence;	
				if (tree->values->letter != '\0')
				{		
						buffer[index].is_leaf = 1;
						buffer[index].letter = tree->values->letter;
				}
				if(tree->left_node != NULL)
				{	
						ft_fill(tree->left_node, buffer, index+1);
				}
				if(tree->right_node != NULL)
				{	
						ft_fill(tree->right_node, buffer, nb_nodes(tree->left_node) + index + 1);
				}
		}
		else
		{
				buffer[index].occurence = -1;		
		}
}

void write_tree_in_file(char *path, nd_h tree)
{
		FILE *tree_file = fopen(path, "w");
		int buffer_size = nb_nodes(tree) * sizeof(ft);
		ft *buffer = malloc(buffer_size);
		memset(buffer, 0, buffer_size);
		ft_fill(tree, buffer, 0);
		fwrite(buffer, 1, buffer_size, tree_file);
		fclose(tree_file);
		free(buffer);
}

nd_h deserialize_node(ft *tree_file, int *index)
{
		ft ft_node = tree_file[*index];
		nd_h node = create_node_huffman(create_tuple(ft_node.letter, ft_node.occurence));
		if(!ft_node.is_leaf)
		{
				int index2 = *index + 1;
				node->left_node = deserialize_node(tree_file, &index2);
				index2++;
				node->right_node = deserialize_node(tree_file, &index2);
				*index = index2;
		}
		return node;

}

nd_h read_ft_tree(ft *tree_file)
{
		int index = 0;
		return deserialize_node(tree_file, &index);
}

nd_h read_tree_file(char *path)
{
		ssize_t file_length = 0;
		ft *tree_file = (ft *)readfile(path, &file_length);	
		if(tree_file == NULL)
		{
				printf("Impossible de lire le fichier !\n");
				return NULL;
		}
		nd_h res = read_ft_tree(tree_file);
		free(tree_file);
		return res;
}

char *get_last_chars(char *src,int pos,int len) { 
		char *res = malloc (len);                        
		memcpy(res, src+pos, len);
		return res;                            
}

char *decompress(char *compressed, ssize_t compressionSize, nd_h tree)
{
	int size_string = sizeof(char) + 1;
	char *res = malloc(size_string);
	memset(res, 0, size_string);
	nd_h actual = tree;
	
	for(ssize_t i = 0; i < compressionSize; i++)
	{
		int index = i / 8;
		if(BIT(compressed[index], i % 8) == 1)
		{
			actual = actual->right_node;
		}
		else
		{
			actual = actual->left_node;
		}
		if(is_leaf(actual))
		{
			res[size_string-2] = actual->values->letter; 
			actual = tree;
			size_string++;
			res = realloc(res, size_string);
			res[size_string-1] = '\0';
		}
	}
	return res;
}

char *read_huff_file(char *path, nd_h tree)
{
		ssize_t file_length = 0;
		char *huff = readfile(path, &file_length);
		if(huff == NULL)
		{
				printf("Impossible de lire le fichier !\n");
				return NULL;
		}
		ssize_t compressionSize;
		char *dest = malloc(sizeof(ssize_t)+1);
		memset(dest, 0, sizeof(ssize_t)+1);
		strncpy(dest, huff, 8);
		memcpy(&compressionSize, dest, sizeof(ssize_t));
		char *textcompressed = get_last_chars(huff, sizeof(ssize_t), file_length - sizeof(ssize_t));
		char *res = decompress(textcompressed, compressionSize, tree);	
		free(huff);
		free(dest);
		free(textcompressed);
		return res;
}
