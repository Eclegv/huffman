#include "weight.h"

int weight(nd_h node)
{
		int res = 0;
		if(node->left_node != NULL && node->right_node != NULL)
		{
				res += node->values->occurence;
				res += weight((node->left_node));
				res += weight((node->right_node));
		}
		return res;
}

