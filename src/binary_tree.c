#include "binary_tree.h"

struct node
{
	nd right_node;
	nd left_node;
	int value;
};

void print_tree(const nd tree)
{
	if (tree != NULL)
	{
		printf("%d \n", tree->value);
		if(tree->left_node != NULL)
			print_tree(tree->left_node);
		if(tree->right_node != NULL)
			print_tree(tree->right_node);
	}
}

nd create_tree(int value)
{
	nd root = (nd) malloc(sizeof(struct node));
	root->value = value;
	root->right_node = NULL;
	root->left_node = NULL;
	return root;
}

void free_tree(nd* tree)
{
	if ( *tree != NULL)
	{
		if ( (*tree)->value > 1 )
		{
			free_tree( &((*tree)->right_node) );
			free_tree( &((*tree)->left_node) );
		}
		free(*tree);
		*tree = NULL;
	}
}

int is_perfect_tree(const nd tree)
{
	if (tree != NULL)
	{
		int is_perfect = 1;
		if (tree->right_node == NULL && tree->left_node == NULL)
		{
			return is_perfect;
		}
		if (tree->right_node != NULL && tree->left_node != NULL)
		{
			is_perfect = is_perfect_tree(tree->left_node);
			if (is_perfect == 0)
			{
				return 0;
			}
			is_perfect = is_perfect_tree(tree->right_node);
		}
		return 0;
	}
	return 0;
}

void add_node(nd tree, int value)
{
	if (tree != NULL)
	{
		if (value < tree->value)
			if (tree->left_node != NULL)
				add_node(tree->left_node, value);
			else
				tree->left_node = create_tree(value);
		else if (value > tree->value)
			if (tree->right_node != NULL)
				add_node(tree->right_node, value);
			else
				tree->right_node = create_tree(value);
		else
			puts("La valeur existe déjà dans l'arbre");
	}
}

int is_leave(nd tree)
{
	if (tree != NULL)
		if (tree->right_node == NULL && tree->left_node == NULL)
			return 1;
	return 0;
}

nd get_node_relative_of_value(nd tree, int value)
{
	if (tree != NULL)
	{
		if (tree->value != value)
		{
			nd node = NULL;
			if (tree->left_node != NULL)
			{
				if (tree->left_node->value == value)
					return tree;
				else
				{
					node = get_node_relative_of_value(tree->left_node, value);
					if (node->left_node != NULL && node->left_node->value == value)
						return node;
					if (node->right_node != NULL && node->right_node->value == value)
						return node;
				}
			}
			if (tree->right_node != NULL)
			{
				if (tree->right_node->value == value)
					return tree;
				else
				{
					node = get_node_relative_of_value(tree->right_node, value);
					if (node->left_node != NULL && node->left_node->value == value)
						return node;
					if (node->right_node != NULL && node->right_node->value == value)
						return node;
				}
			}
		}
	}
	return tree;
}

nd get_min(nd tree)
{
	if (tree != NULL)
	{
		nd min = NULL;
		if (tree->left_node != NULL)
		{
			if (get_min(tree->left_node)->value <= min->value)
			{
				min = get_min(tree->left_node);
				min = get_node_relative_of_value(tree, min->value);
			}
		}
		return min;
	}
	return NULL;
}

nd get_max(nd tree)
{
	if (tree != NULL)
	{
		nd max = NULL;
		if (tree->right_node != NULL)
		{
			if (get_max(tree->left_node)->value <= max->value)
			{
				max = get_max(tree->left_node);
				max = get_node_relative_of_value(tree, max->value);
			}
		}
		return max;
	}
	return NULL;
}

void remove_node_two_children(nd tree, nd node)
{
	if (tree != NULL)
	{
		nd node_min = NULL;
		nd node_max = NULL;
		if (tree->left_node != NULL && tree->left_node->value == node->value)
			node_min = get_min(node->right_node);
		if (tree->right_node != NULL && tree->right_node->value == node->value)
			node_max = get_max(node->left_node);
		if (node_min != NULL)
		{
			nd true_min = node_min->left_node;
			if (true_min->right_node != NULL)
			{
				node_min->left_node = true_min->right_node;
				true_min->right_node = NULL;
			}
			else
				node_min->left_node = NULL;
			true_min->left_node = node->left_node;
			node->left_node = NULL;
			true_min->right_node = node->right_node;
			node->right_node = NULL;
			tree->left_node = true_min;
			free(node);
			node = NULL;
		}
		if (node_max != NULL)
		{
			nd true_max = node_max->right_node;
			if (true_max->left_node != NULL)
			{
				node_max->right_node = true_max->left_node;
				true_max->left_node = NULL;
			}
			else
				node_max->right_node = NULL;
			true_max->left_node = node->left_node;
			node->left_node = NULL;
			true_max->right_node = node->right_node;
			node->right_node = NULL;
			tree->right_node = true_max;
			free(node);
			node = NULL;
		}
	}
}

void remove_node_one_child(nd tree, nd node)
{
	if (node->left_node != NULL)
	{
		if (tree->left_node->value == node->value)
		{
			tree->left_node = node->left_node;
			free(node);
			node = NULL;
		}
		else
		{
			tree->right_node = node->left_node;
			free(node);
			node = NULL;
		}
	}
	else
	{
		if (tree->left_node->value == node->value)
		{
			tree->left_node = node->right_node;
			free(node);
			node = NULL;
		}
		else
		{
			tree->right_node = node->right_node;
			free(node);
			node = NULL;
		}
	}
}

void remove_leave(nd node)
{
	if (node->left_node != NULL)
	{
		if (is_leave(node->left_node) == 1)
		{
			free(node->left_node);
			node->left_node = NULL;
		}
	}
	if (node->right_node != NULL)
	{
		if (is_leave(node->right_node) == 1)
		{
			free(node->right_node);
			node->right_node = NULL;
		}
	}
}

int have_children(nd tree)
{
	if (tree != NULL)
		if (tree->left_node != NULL && tree->right_node != NULL)
			return 1;
	return 0;
}

int have_only_one_child(nd tree)
{
	if ( (tree->left_node != NULL && tree->right_node == NULL) || (tree->left_node == NULL && tree->right_node != NULL) )
		return 1;
	return 0;
}

void remove_node(nd tree, int value)
{
	if (tree != NULL)
	{
		nd delete_node = get_node_relative_of_value(tree, value);
		if (delete_node->value != value)
		{
			if (delete_node->left_node != NULL && delete_node->left_node->value == value)
			{
				if (have_only_one_child(delete_node->left_node) == 1)
					remove_node_one_child(delete_node, delete_node->left_node);
				else if (have_children(delete_node->left_node) == 1)
					remove_node_two_children(delete_node, delete_node->left_node);
				else
					remove_leave(delete_node);
			}
			if (delete_node->right_node != NULL && delete_node->right_node->value == value)
			{
				if (have_only_one_child(delete_node->right_node) == 1)
					remove_node_one_child(delete_node, delete_node->right_node);
				else if (have_children(delete_node->right_node) == 1)
					remove_node_two_children(delete_node, delete_node->right_node);
				else
					remove_leave(delete_node);
			}
		}
	}
}
