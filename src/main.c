// #include "binary_tree.h"
#include "huffman_tree.h"
#include "weight.h"

char	*strnew(size_t size)
{
	char *str;

	if ((str = (char *)malloc(size + 1)) == NULL)
		return NULL;
	memset(str, 0, size+1);
	return str;
}

char	*strjoin(char *s1, char *s2)
{
	size_t s1len;
	size_t len;
	size_t i;
	char *str;

	if (!s1 || !s2)
		return NULL;
	s1len = strlen(s1);
	len = s1len + strlen(s2);
	if ((str = strnew(len)) == NULL)
		return NULL;
	i = 0;
	while (i < s1len)
	{
		str[i] = s1[i];
		i++;
	}
	while (i < len)
	{
		str[i] = s2[i - s1len];
		i++;
	}
	return str;
}

int main(int argc, char** argv)
{
	if(argc < 3 && argc > 4)
	{
		return 1;
	}
	else
	{
		if(strcmp("compress", argv[1]) == 0)
		{
			printf("Compressing: %s\n", argv[2]);
			ssize_t trash = 0;
			char* text = readfile(argv[2], &trash);
			if (text == NULL)
			{
				printf("Failed to read file: %s\n", argv[2]);
				//printf("%d\n", errno);
				return 1;
			}
			nd_h *root = create_tree_huffman(text, &weight);
			print_tree_huffman(root[0],  0);
			int len = different_letters(text);
			int len2 = 0;
			char *huff_filename = strjoin(argv[2], ".huff");
			char *tree_filename = strjoin(huff_filename, ".tree");
			lcomp *prefix = list_compression(root[0], '\0', &len2, depth(root[0]));
			reverse_compressions(prefix, len);
			print_list_compression(prefix, len);
		
			unsigned long compressionSize;
			int arraySize;
			char *buffer = prepareCompressed(prefix, len, &compressionSize, &arraySize, text);
			write_in_file(huff_filename, buffer, compressionSize, arraySize);
			printf("File saved to %s\n", huff_filename);		
	
			write_tree_in_file(tree_filename, root[0]);		
			printf("File saved to %s\n", tree_filename);		
	

			free(huff_filename);
			huff_filename = NULL;
			free(tree_filename);
			tree_filename = NULL;
			free(buffer);
			free(text);
			text = NULL;
			free_tree_huffman(&root[0]);
			free(root);
			free_list_compression(prefix, len);
			prefix = NULL;
		}
		else if(strcmp("decompress", argv[1]) == 0)
		{
			char *tree_filename = strjoin(argv[2], ".tree");
			nd_h root = read_tree_file(tree_filename);
			print_tree_huffman(root,  0);
			char *decompression = read_huff_file(argv[2], root);
			printf("\nText out:\n %s\n", decompression);

			free(tree_filename);
			tree_filename = NULL;
			free_tree_huffman(&root);
			free(decompression);
		}
	}
	// printf("--------\n");
	// nd tree = create_tree(4);
	// print_tree(tree);
  //
	// printf("Est parfait : %d\n", is_perfect_tree(tree));
  //
	// add_node(tree, 2);
	// add_node(tree, 6);
	// add_node(tree, 1);
	// add_node(tree, 3);
	// add_node(tree, 5);
	// add_node(tree, 7);
	// print_tree(tree);
  //
	// puts("");
  //
	// remove_node(tree, 2);
	// print_tree(tree);
  //
	// free_tree(&tree);
	puts("");

	return 0;
}
