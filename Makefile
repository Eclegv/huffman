# compilateur
CC := gcc
# options de compilation
CFLAGS := -std=c99 -Wall -Wextra -Werror -pedantic -ggdb -lm
INCLUDE := -I include

SRCS=$(wildcard src/*.c)
OBJS=$(SRCS:src/%.c=obj/%.o)

.PHONY: all run clean prepare

# règle de compilation --- exécutables
all: prepare bin/HuffMan

bin/HuffMan: $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^

obj/%.o: src/%.c
	$(CC) $(CFLAGS) -o $@ -c $< $(INCLUDE)

# options de compilation
run: all
	./bin/HuffMan $(COMP) $(VAR) $(VAR2)

run_valgrind: all
	valgrind ./bin/HuffMan

clean:
	rm -rf bin obj lib

prepare:
	test -d bin || mkdir bin
	test -d obj || mkdir obj
