#ifndef BINARY_TREE_H
#define BINARY_TREE_H


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define BUFSIZE 1024

struct tuple
{
		char letter;
		int occurence;
};
typedef struct tuple* tpl;

struct node_huffman
{
		struct node_huffman* left_node;
		struct node_huffman* right_node;
		tpl values;
};

typedef struct letter_compression
{
		char letter;
		char *compression;
} lcomp;

typedef struct file_tree
{
		int occurence;
		unsigned char is_leaf;
		char letter;
} ft;


typedef struct node_huffman* nd_h;

tpl create_tuple(char letter, int occurence);
nd_h create_node_huffman(tpl values);

nd_h *create_tree_huffman(char* sentence, int(*weight_ptr)(nd_h));
int different_letters(char* string);

void print_tree_huffman(nd_h tree, int space);
void free_tree_huffman(nd_h* tree);
int depth(nd_h tree);

void free_list_compression(lcomp *prefix, int len);
void reverse_compressions(lcomp *compressions, int len);
char* readfile(char* filepath, ssize_t *contentlength);
lcomp *list_compression(nd_h tree, char direction, int *len, int depth);
void print_list_compression(lcomp *res, int len);

void write_tree_in_file(char *path, nd_h tree);
char *prepareCompressed(lcomp *compressions, int len, unsigned long *compressionSize, int *arraySize, char* text);
void write_in_file(char *path, char *buffer, unsigned long compressionSize, int arraySize);
nd_h read_tree_file(char *path);
char *read_huff_file(char *path, nd_h tree);
#endif
