#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <stdio.h>
#include <stdlib.h>


typedef struct node* nd;

nd create_tree(int value);
void free_tree(nd* tree);
int is_perfect_tree(const nd tree);
void print_tree(const nd tree);

void add_node(nd tree, int value);
void remove_node(nd tree, int value);
int is_leave(nd tree);
nd* list_leaves(nd tree);

#endif
